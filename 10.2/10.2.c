#include<stdio.h>
#define N 5

struct employee
{
	char name[20];
	char id[20];
	int gl;
	float salary;//工资
}

int main()
{
	int i,j=0;
	float max=0;//工资用int或float；
	struct employee em[N];

	
	for(i=0;i<N;i++)
	{
		printf("输入第 %d 员工资料(姓名，身份ID，工龄，工资[以回车分割])：\n",i+1);
		gets(em[i].name);
		gets(em[i].id);
		scanf("%d",&em[i].gl);
		scanf("%f",&em[i].salary);
	}
	
	max=em[0].salary;
	
	for(i=0; i<N; i++)
	{
		if(max < em[i].salary)max=em[i].salary;
	}
	
	printf("员工中的最高工资是:%6.2f\n",max);
	
	return 0;
}